# Marlin Prova

Prova está na feature de develop !

Execute Primeiro o Script que está na pasta com o nome de : Script database Marlin, para poder criar as Tabelas.
Antes de executar o Projeto pela Primeira Vez, eu utilizo o usuário do Ms/Sql com o nome de sa, e a senha de root,
Caso você não utilize esses usuários como padrões, Altere o Arquivo: appsettings.Development.json e 
appsettings.json informando o seu usuário e senha do banco de dados.

O arquivo se encontra no projeto Marlin.WebApi. 
A Linha a ser editada seria esta: 
"DefaultConnection": "Server=;Database=Marlin;Trusted_Connection=True;MultipleActiveResultSets=true;User ID=sa;Password=root".
Onde está escrito: User ID=sa, coloque o seu Usuário, e onde está escrito Password=root, coloque sua senha do SqlServer.
A Aplicação irá fazer um update-database automático para criar as tabelas do Banco de Dados do Identity.
Na descricação do Projeto, foi informado para fazer a importação das tabelas utilizando o Entity Data Model (.edmx).
Como o EF Core não tem suporte ao Entity Data Model, utilizei o processo de engenharia reversa, onde ele mapeia o banco de dados e cria as tabelas do banco de dados 
Automaticamente pelo seguinte comando:
Scaffold-DbContext "Server=;Database=Marlin;Trusted_Connection=True;MultipleActiveResultSets=true;User ID=sa;Password=root" Microsoft.EntityFrameworkCore.SqlServer -OutputDir 
Criei outros métodos além dos que estavam descritos para poder me ajudar a fazer todas as regras de negócio informadas no Email.

Para poder utilizar o swagger,  faça login com o login: admin e password: 123
Feito Login, a Api irá retorna um Token, copie este Token, e no botão escrito Authorize, que está localizado no canto superior direito da tela escreva: Bearer (token).
Exemplo: caso o token retornado seja 123, coloque: Bearer 123.
Isso irá fazer com que você consiga utilizar o swagger sem problema, pois ele irá armazenar o token.
Coloquei o Token com duração de 30 minutos.


