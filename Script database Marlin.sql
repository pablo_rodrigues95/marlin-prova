CREATE DATABASE Marlin
GO

USE Marlin
go

CREATE TABLE Turmas (
IdTurma int primary key not null IDENTITY,
Descricao nvarchar(max) not null)
go

CREATE TABLE Usuarios(
IdUsuario int primary key not null IDENTITY ,
Nome nvarchar(max) not null,
Login nvarchar(max) not null,
Senha nvarchar(max) not null,
Email nvarchar(max) not null,)
go

CREATE TABLE Alunos(
IdAluno int primary key not null IDENTITY,
Matricula nvarchar(max) not null, 
Nome nvarchar(max) not null,
Email nvarchar(max) not null, 
IdTurmaAluno int)
go

ALTER TABLE Alunos 
	ADD CONSTRAINT Fk_TurmaAluno FOREIGN KEY (IdTurmaAluno) REFERENCES Turmas (IdTurma) ON DELETE CASCADE
      ON UPDATE CASCADE
	  go

INSERT INTO dbo.Turmas (Descricao)
Values ('basico'),('intermediario'), ('avancado');
go

INSERT INTO dbo.Usuarios (Nome,Login,Senha,Email) 
Values('admin','admin','202CB962AC59075B964B07152D234B70','admin@gmail.com');
go




