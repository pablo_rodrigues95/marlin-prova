﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marlin.Domain
{
    public interface IUsuarioRepository
    {
        Task Incluir(Usuarios usuario);
        Task<Usuarios> Login(string usuario, string senha);
        
    }
}
