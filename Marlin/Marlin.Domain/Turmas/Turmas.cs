﻿using System;
using System.Collections.Generic;

namespace Marlin.Domain
{
    public partial class Turmas
    {
        public Turmas()
        {
            Alunos = new HashSet<Alunos>();
        }

        public int IdTurma { get; set; }
        public string Descricao { get; set; }

        public virtual ICollection<Alunos> Alunos { get; set; }
    }
}
