﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marlin.Domain
{
    public interface ITurmaRepository
    {
        Task<Turmas> BuscarTurmaPorId(int Id);
        Task<Turmas> BuscarTurmaPorNome(string Nome);
        Task<Boolean> VerificarTurmaCheia(string NomeTurma);
    }
}
