﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marlin.Domain.Aluno
{
    public interface IAlunoRepository
    {
        Task Incluir(Alunos aluno);
        Task Alterar(Alunos aluno);
        Task Excluir(Alunos aluno);
        Task ExcluirAlunoTurma(Alunos aluno);
        Task<Alunos> BuscarPorId(int Id);
        Task<Alunos> BuscarPorMatricula(string matricula);
        Task<List<Alunos>> Todos();
        
    }
}
