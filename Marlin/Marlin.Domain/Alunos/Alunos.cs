﻿using System;
using System.Collections.Generic;

namespace Marlin.Domain
{
    public partial class Alunos
    {
        public int IdAluno { get; set; }
        public string Matricula { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }

        public int? IdTurmaAluno { get; set; }

        public virtual Turmas IdTurmaAlunoNavigation { get; set; }
    }
}
