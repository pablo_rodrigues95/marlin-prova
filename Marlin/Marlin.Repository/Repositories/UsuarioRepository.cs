﻿using Marlin.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Marlin.Repository.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly MarlinContext _context;
        public UsuarioRepository(MarlinContext context)
        {
            _context = context;
        }
        public async Task Incluir(Usuarios usuario)
        {
            await _context.Usuarios.AddAsync(usuario);
            await _context.SaveChangesAsync();
        }

        public async Task<Usuarios> Login(string usuario, string senha) => 
            await _context.Usuarios.FirstOrDefaultAsync(a => a.Login == usuario && a.Senha == senha);
        
    }
}
