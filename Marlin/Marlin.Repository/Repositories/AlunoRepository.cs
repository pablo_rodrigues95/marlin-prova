﻿using Marlin.Domain;
using Marlin.Domain.Aluno;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marlin.Repository.Repositories
{
    public class AlunoRepository : IAlunoRepository
    {
        private readonly MarlinContext _context;
        
        public AlunoRepository(MarlinContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
           
        }
        public async Task Alterar(Alunos aluno)
        {
            var alunoAntigo = await BuscarPorId(aluno.IdAluno);
            alunoAntigo.Matricula = aluno.Matricula;
            alunoAntigo.Nome = aluno.Nome;
            alunoAntigo.Email = aluno.Email;
            alunoAntigo.IdTurmaAluno = aluno.IdTurmaAluno;
            if (aluno.IdTurmaAluno != null)
            {
                alunoAntigo.IdTurmaAlunoNavigation = await _context.Turmas.FirstOrDefaultAsync(t => t.IdTurma == aluno.IdTurmaAluno);
            }
            else
            {
                alunoAntigo.IdTurmaAlunoNavigation = null;
            }
            _context.Alunos.Attach(alunoAntigo);
            _context.Entry(alunoAntigo).State = EntityState.Modified;

           await _context.SaveChangesAsync();
        }

        public async Task<Alunos> BuscarPorId(int Id) => await _context.Alunos.FirstOrDefaultAsync(a => a.IdAluno == Id);

        public async Task<Alunos> BuscarPorMatricula(string matricula) => await _context.Alunos.FirstOrDefaultAsync(a => a.Matricula == matricula);
        
        public async Task Excluir(Alunos aluno)
        {
            _context.Alunos.Remove(aluno);
            await _context.SaveChangesAsync();
        }

        public async Task ExcluirAlunoTurma(Alunos aluno)
        {
            var alunoAntigo = await BuscarPorId(aluno.IdAluno);
            alunoAntigo.IdTurmaAluno = null;
            alunoAntigo.IdTurmaAlunoNavigation = null;
            await Alterar(alunoAntigo);
        }

        public async Task Incluir(Alunos aluno)
        {
            await _context.Alunos.AddAsync(aluno);
            await _context.SaveChangesAsync();
        }

        public async Task<List<Alunos>> Todos() => await _context.Alunos.ToListAsync();
        
        
    }
}
