﻿using Marlin.Domain;
using Marlin.Domain.Aluno;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marlin.Repository.Repositories
{
    public class TurmaRepository : ITurmaRepository
    {
        private readonly MarlinContext _context;
        private readonly IAlunoRepository _alunoRepository;
        public TurmaRepository(MarlinContext context, IAlunoRepository alunoRepository)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _alunoRepository = alunoRepository ?? throw new ArgumentNullException(nameof(alunoRepository));
        }

        public async Task<Turmas> BuscarTurmaPorId(int Id) =>
            await _context.Turmas.FirstOrDefaultAsync(t => t.IdTurma == Id);
        public async Task<Turmas> BuscarTurmaPorNome(string Nome) => 
            await _context.Turmas.FirstOrDefaultAsync(t => t.Descricao == Nome);
        public async Task<bool> VerificarTurmaCheia(string nomeTurma)
        {
            var turma = await BuscarTurmaPorNome(nomeTurma);
            List<Alunos> todosalunos = await _alunoRepository.Todos();
            List<Alunos> alunosDaTurma = todosalunos.Where(c => c.IdTurmaAluno == turma.IdTurma).ToList();
            if (alunosDaTurma.Count >= 5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
