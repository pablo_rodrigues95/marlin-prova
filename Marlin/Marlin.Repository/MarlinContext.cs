﻿using System;
using Marlin.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Marlin.Repository
{
    public partial class MarlinContext : DbContext
    {
        public MarlinContext()
        {
        }

        public MarlinContext(DbContextOptions<MarlinContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Alunos> Alunos { get; set; }
        public virtual DbSet<Turmas> Turmas { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=;Database=Marlin;Trusted_Connection=True;MultipleActiveResultSets=true;User ID=sa;Password=root");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alunos>(entity =>
            {
                entity.HasKey(e => e.IdAluno)
                    .HasName("PK__Alunos__8092FCB376FE896F");

                entity.Property(e => e.Matricula).IsRequired();

                entity.Property(e => e.Nome).IsRequired();
                
                entity.Property(e => e.Email).IsRequired();

                entity.HasOne(d => d.IdTurmaAlunoNavigation)
                    .WithMany(p => p.Alunos)
                    .HasForeignKey(d => d.IdTurmaAluno)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("Fk_TurmaAluno");
            });

            modelBuilder.Entity<Turmas>(entity =>
            {
                entity.HasKey(e => e.IdTurma)
                    .HasName("PK__Turmas__C1ECFFC9C6717F97");

                entity.Property(e => e.Descricao).IsRequired();
            });

            modelBuilder.Entity<Usuarios>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("PK__Usuarios__5B65BF97CCE26859");

                entity.Property(e => e.Email).IsRequired();

                entity.Property(e => e.Login).IsRequired();

                entity.Property(e => e.Nome).IsRequired();

                entity.Property(e => e.Senha).IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
