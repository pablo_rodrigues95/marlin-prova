﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Marlin.WebApi.Models
{
    public class UsuarioModel
    {
        [Required(ErrorMessage = "Nome é Obirgatório !")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Login é Obirgatório !")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Senha é Obirgatório !")]
        public string Senha { get; set; }
        [Required(ErrorMessage = "Email é Obirgatório !")]
        public string Email { get; set; }
    }
}
