﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Marlin.WebApi.Models
{
    public class AlunoModel
    {
        [Required(ErrorMessage = "Nome do Aluno é Obrigátorio !")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Email do Aluno é Obrigátorio !")]
        [EmailAddress(ErrorMessage = "Formato de email Inválido")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Matricula do Aluno é Obrigatório !")]
        public string Matricula { get; set; }
        [Required(ErrorMessage = "Nome da Turma é Obrigatório !")]
        public string NomeTurma { get; set; }
    }
}
