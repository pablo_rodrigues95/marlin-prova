﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Marlin.WebApi.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage ="Nome Usuário é obrigatório !")]
        [Display(Name = "Usuário")]
        public string Login { get; set; }
        [Required(ErrorMessage ="Senha é obrigatório !")]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }
    }
}
