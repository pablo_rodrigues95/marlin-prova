﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Marlin.Domain;
using Marlin.WebApi.Infrastructure.Security;
using Marlin.WebApi.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;

namespace Marlin.WebApi.Controllers
{   
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IMapper _mapper;
        private readonly IUsuarioRepository _usuarioRepository;

        public LoginController(IUsuarioRepository usuarioRepository, IMapper mapper, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, ILogger<LoginController> logger)
        {
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _userManager = userManager;
            _mapper = mapper;
            _usuarioRepository = usuarioRepository;
        }

        /// <summary>
        /// para fazer login pela primeira vez, use o usuário: admin , e o password: 123. está como default.
        /// </summary>
        /// <remarks>
        /// Exemplo:
        ///     {
        ///        "login": "admin",
        ///        "password": "123"
        ///     }
        ///
        /// </remarks>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var senha = CriarMd5(login.Password);
                    var usuario = await _usuarioRepository.Login(login.Login, senha);
                    if (usuario != null)
                    {
                        var user = await _userManager.FindByNameAsync(login.Login);

                        if (user != null)
                        {
                            var signInResult = await _signInManager.PasswordSignInAsync(user, senha, false, false);

                            if (signInResult.Succeeded)
                            {
                                return Ok(GerarToken(login));
                            }
                            return BadRequest("Não foi possivel fazer o login em  _signInManager.PasswordSignInAsync");
                        }
                        else
                        {
                            /*PAra criar usuario caso não tenha no Identity !*/
                            var userIdentity = new IdentityUser
                            {
                                UserName = usuario.Login,
                                Email = usuario.Email,
                            };

                            var result = await _userManager.CreateAsync(userIdentity, usuario.Senha);

                            if (result.Succeeded)
                            {
                                //sign in
                                var signInResult = await _signInManager.PasswordSignInAsync(userIdentity, usuario.Senha, false, false);

                                if (!signInResult.Succeeded)
                                {
                                    return BadRequest("Erro ao Criar Usuário no Identity");
                                }
                                return Ok(GerarToken(login));
                            }
                            return BadRequest("Erro ao Criar Usuário no Identity");
                        }
                    }
                    return BadRequest("Usuário do modelo Nulo !");

                }
                catch (Exception ex)
                {
                    return BadRequest("Excecao - " + ex);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpPost]
        [Route("{action}")]
        public async Task<IActionResult> CriarUsuario([FromBody] UsuarioModel model)
        {
            if (ModelState.IsValid)
            {
                var usuario = _mapper.Map<Usuarios>(model);
                usuario.Senha = CriarMd5(model.Senha);
                try
                {
                   await _usuarioRepository.Incluir(usuario);
                    var user = new IdentityUser
                    {
                        UserName = usuario.Login,
                        Email = usuario.Email,
                    };

                    var result = await _userManager.CreateAsync(user, usuario.Senha);

                    if (result.Succeeded)
                    {
                        //sign in
                        var signInResult = await _signInManager.PasswordSignInAsync(user, usuario.Senha, false, false);

                        if (signInResult.Succeeded)
                        {
                            return StatusCode(201,"Usuário CRiado com Susceeso !");
                        }
                    }

                }
                catch (Exception ex)
                {
                    return BadRequest("Excecao - " + ex.InnerException.Message);
                }

            }
            return BadRequest(ModelState);

        }

        private UserToken GerarToken(LoginModel login)
        {
            var direitos = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, login.Login),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var chave = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(KeyToken.ChaveToken));
            var credenciais = new SigningCredentials(chave, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                claims: direitos,
                signingCredentials: credenciais,
                expires: DateTime.Now.AddMinutes(30)
            );
            var tokenGerado = new UserToken();
            tokenGerado.Token = new JwtSecurityTokenHandler().WriteToken(token);
            tokenGerado.Expiration = DateTime.Now.AddMinutes(30);
            return tokenGerado;
        }

        public static string CriarMd5(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}