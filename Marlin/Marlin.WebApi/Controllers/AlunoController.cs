﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Marlin.Domain;
using Marlin.Domain.Aluno;
using Marlin.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Marlin.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AlunoController : ControllerBase
    {
        private readonly IAlunoRepository _alunoRepository;
        private readonly ITurmaRepository _turmaRepository;
        private readonly ILogger<AlunoController> _logger;
        private readonly IMapper _mapper;

        public AlunoController(IAlunoRepository alunoRepository, ILogger<AlunoController> logger, ITurmaRepository turmaRepository, IMapper mapper)
        {
            _alunoRepository = alunoRepository ?? throw new ArgumentNullException(nameof(alunoRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _turmaRepository = turmaRepository ?? throw new ArgumentNullException(nameof(turmaRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Para alterar o Aluno, informe a Matricula Correta Do Aluno, os aoutros campos, voce poderá editar ! 
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> AlteracaoAluno([FromBody] AlunoModel model)
        {
            if (ModelState.IsValid)
            {
                var AlunoMatricula = await _alunoRepository.BuscarPorMatricula(model.Matricula);
                if (AlunoMatricula == null)
                {
                    ModelState.AddModelError(model.Matricula, $"Aluno não encontrado com a matrícula {model.Matricula} !");
                    return BadRequest(ModelState);
                }

                var turma = await _turmaRepository.BuscarTurmaPorNome(model.NomeTurma);
                if (turma == null)
                {
                    ModelState.AddModelError(model.NomeTurma, $"Turma {model.NomeTurma}  não encontrada !");
                    return BadRequest(ModelState);
                }

                var aluno = _mapper.Map<Alunos>(model);
                aluno.IdAluno = AlunoMatricula.IdAluno;
                aluno.IdTurmaAluno = turma.IdTurma;   
                try
                {
                    aluno.IdTurmaAlunoNavigation = AlunoMatricula.IdTurmaAlunoNavigation;
                    await _alunoRepository.Alterar(aluno);
                    return Ok("Aluno alterado com sucesso !");
                }
                catch (Exception ex)
                {
                    _logger.LogError("AlteracaoAluno", ex);
                    return BadRequest("Erro ao alterar no banco de dados ! " + ex.InnerException.Message);
                }
            }
            return BadRequest();
        }
        /// <summary>
        /// Para Cadastrar o Aluno, informe os campos informados Abaixo ! 
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CadastroAluno([FromBody] AlunoModel model)
        {
            if (ModelState.IsValid)
            {
                var turmaEncontrada = await _turmaRepository.BuscarTurmaPorNome(model.NomeTurma);
                var alunoRepetido =  await _alunoRepository.BuscarPorMatricula(model.Matricula);
                if (alunoRepetido != null)
                {
                    ModelState.AddModelError(model.Matricula, $"Matricula já está associado a outro aluno. Aluno: " +
                        $"{alunoRepetido.Nome}, matricula {model.Matricula}");
                    return BadRequest(ModelState);
                }
                if (turmaEncontrada == null)
                {
                    ModelState.AddModelError(model.NomeTurma, "Turma não encontrada !");
                    return BadRequest(ModelState);
                }
                if (await _turmaRepository.VerificarTurmaCheia(turmaEncontrada.Descricao))
                {
                    ModelState.AddModelError(model.NomeTurma, "Turma já possui 5 alunos !");
                    return BadRequest(ModelState);
                }
                var aluno = _mapper.Map<Alunos>(model);
                aluno.IdTurmaAluno = turmaEncontrada.IdTurma;
                try
                {
                    await _alunoRepository.Incluir(aluno);
                    return StatusCode(201,"Aluno criado com sucesso !");
                }
                catch (Exception ex)
                {
                    _logger.LogError("CadastroAluno", ex);
                    return BadRequest("Erro ao cadastrar Aluno na Base de Dados ! " + ex);
                    throw;
                }

            }
            else
            {
                return BadRequest();
            }     
        }

        [HttpDelete]
        [Route("{matricula}")]
        public async Task<IActionResult> ExclusaoAluno(string matricula)
        {
            var AlunoMatricula = await _alunoRepository.BuscarPorMatricula(matricula);
            if (AlunoMatricula == null)
            {
                ModelState.AddModelError(matricula, "Aluno não encontrado com a matrícula informada");
                return BadRequest(ModelState);
            }
            if (AlunoMatricula.IdTurmaAluno != null)
            {
                var id = AlunoMatricula.IdTurmaAluno ?? default(int);
                var turma = await _turmaRepository.BuscarTurmaPorId(id);
                ModelState.AddModelError(AlunoMatricula.IdTurmaAluno.ToString(),
                    $"Aluno está associado a Turma: {turma.Descricao}");
                return BadRequest(ModelState);
            }
            var aluno = await _alunoRepository.BuscarPorMatricula(matricula);
            try
            {
                await _alunoRepository.Excluir(aluno);
                return StatusCode(203,"Aluno Excluido com sucesso !");
            }
            catch (Exception ex)
            {
                _logger.LogError("ExclusaoAluno", ex);

                return BadRequest("Erro ao excluir aluno na Base de Dados !");
            }
            
        }
        
        [HttpDelete]
        [Route("ExclusaoAlunoTurma/{matricula}")]
        public async Task<IActionResult> ExclusaoAlunoTurma([FromRoute] string matricula)
        {
            var AlunoMatricula = await _alunoRepository.BuscarPorMatricula(matricula);
            if (AlunoMatricula == null)
            {
                ModelState.AddModelError(matricula, "Aluno não encontrado com a matrícula informada");
                return BadRequest(ModelState);
            }

            var turma = await _turmaRepository.BuscarTurmaPorId(AlunoMatricula.IdTurmaAluno ?? default(int));

            try
            {
                await _alunoRepository.ExcluirAlunoTurma(AlunoMatricula);
                return Ok($"Aluno :{AlunoMatricula.Nome} foi removido da turma: {turma.Descricao} com sucesso !");
            }
            catch (Exception ex)
            {
                _logger.LogError("ExclusaoAlunoTurma", ex.InnerException.Message);
                return BadRequest("Erro ao excluir a Turma no Banco de dados !" + ex.InnerException.Message);
            }
        }
    }
}