﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marlin.WebApi.Infrastructure.ExtensionMethods
{
    public static class AutoMapperExtension
    {
        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Models.AlunoModel, Domain.Alunos>();
                cfg.CreateMap<Models.UsuarioModel, Domain.Usuarios>();

            });

            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            return services;
        }
    }
}
