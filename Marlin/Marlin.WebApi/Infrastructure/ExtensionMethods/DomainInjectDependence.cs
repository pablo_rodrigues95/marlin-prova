﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marlin.WebApi.Infrastructure
{
    public static class DomainInjectDependence
    {
        public static IServiceCollection AddDomain(this IServiceCollection services)
        {
            services.AddTransient<Domain.ITurmaRepository, Repository.Repositories.TurmaRepository>();
            services.AddTransient<Domain.Aluno.IAlunoRepository, Repository.Repositories.AlunoRepository>();
            services.AddTransient<Domain.IUsuarioRepository, Repository.Repositories.UsuarioRepository>();

            return services;
        }
    }
}
